﻿using System;
using System.IO;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made re-distributable by Microsoft
using System.Reflection;
using System.Threading.Tasks;
using Modern.Forms;
using SkiaSharp;

namespace Ssepan.Application.Core.ModernForms
{
    /// <summary>
    /// Note: Several static methods are re-implemented as async methods
    /// due to the fact that MessageBox.Modern.Forms's Show[...] methods force async/await.
    /// </summary>
    public static class Dialogs
    {
        #region Declarations
        public const string FilterSeparator = "|";
        public const string FilterDescription = "{0} Files(s)";
        public const string FilterFormat = "{0} (*.{1})|*.{1}";
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static. Calls Modern.Forms.SaveFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo"></param>
        /// <returns>fileDialogInfo</returns>
        public async static Task<FileDialogInfo<Form, DialogResult>> GetPathForSave
        (
            FileDialogInfo<Form, DialogResult> fileDialogInfo
        )
        {
			bool returnValue;
			try
			{
				if
				(
					fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
					||
					fileDialogInfo.ForceDialog
				)
				{
					SaveFileDialog saveFileDialog =
			new()
			{
				Title = fileDialogInfo.ForceDialog ? "Save As..." : "Save...",
				InitialDirectory =

					fileDialogInfo.InitialDirectory == default
					?
					fileDialogInfo.CustomInitialDirectory
					:
					Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
				,
				// Console.WriteLine("GetPathForSave:fileDialogInfo.InitialDirectory"+fileDialogInfo.InitialDirectory);
				// Console.WriteLine("GetPathForSave:fileDialogInfo.CustomInitialDirectory"+fileDialogInfo.CustomInitialDirectory);
				// Console.WriteLine("GetPathForSave:fileDialog.Directory"+fileDialog.Directory);

				FileName = fileDialogInfo.Filename
			};

					foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Form, DialogResult>.FILTER_SEPARATOR))
					{
						string[] nameAndPattern = filter.Split(FileDialogInfo<Form, DialogResult>.FILTER_ITEM_SEPARATOR);
						saveFileDialog.AddFilter(nameAndPattern[0], nameAndPattern[1]);
					}

					DialogResult response = await saveFileDialog.ShowDialog(fileDialogInfo.Parent);

					if (response != DialogResult.None)
					{
						if (response == DialogResult.OK)
						{
							if (string.Equals(Path.GetFileName(saveFileDialog.FileName), fileDialogInfo.NewFilename, StringComparison.CurrentCultureIgnoreCase))
							{
								//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
								string messageTemp = string.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
								MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> messageDialogInfo =
						new (
							fileDialogInfo.Parent,
							fileDialogInfo.Modal,
							fileDialogInfo.Title,
							null,
							ImageLoader.Get("Info.png"),
							DialogBoxForm.ButtonTypes.OK,
							messageTemp,
							DialogResult.None
						);
								messageDialogInfo = await ShowMessageDialog(messageDialogInfo);

								//Forced cancel
								fileDialogInfo.Response = DialogResult.Cancel;
							}
							else
							{
								//set new filename
								fileDialogInfo.Filename = saveFileDialog.FileName;
								fileDialogInfo.Filenames = [saveFileDialog.FileName];
								fileDialogInfo.Response = response;
								returnValue = true;
							}
						}
						fileDialogInfo.Response = response;
						returnValue = true;
					}
					else
					{
						//treat as Cancel
						fileDialogInfo.Response = DialogResult.Cancel;
						returnValue = true;
					}
				}
				else
				{
					fileDialogInfo.Response = DialogResult.OK;
					returnValue = true;
				}
			}
			catch (Exception ex)
			{
				fileDialogInfo.ErrorMessage = ex.Message;
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

				throw;
			}

			fileDialogInfo.BoolResult = returnValue;
            return fileDialogInfo;
        }

        /// <summary>
        /// Get path to load data.
        /// Static. Calls Modern.Forms.OpenFileDialog.
        /// </summary>
        /// <param name="fileDialogInfo">FileDialogInfo<Form, DialogResult></param>
        /// <returns>Task<FileDialogInfo<Form, DialogResult>></returns>
        public async static Task<FileDialogInfo<Form, DialogResult>> GetPathForLoad
        (
            FileDialogInfo<Form, DialogResult> fileDialogInfo
        )
        {
			bool returnValue;
			try
			{
				if (fileDialogInfo.ForceNew)
				{
					fileDialogInfo.Filename = fileDialogInfo.NewFilename;

					returnValue = true;
				}
				else
				{
					OpenFileDialog openFileDialog =
			new()
			{
				Title = fileDialogInfo.Title,
				InitialDirectory =

					fileDialogInfo.InitialDirectory == default
					?
					fileDialogInfo.CustomInitialDirectory
					:
					Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
				,
				FileName = fileDialogInfo.Filename,
				AllowMultiple = fileDialogInfo.Multiselect
			};

					// Console.WriteLine("GetPathForLoad:fileDialogInfo.InitialDirectory"+fileDialogInfo.InitialDirectory);
					// Console.WriteLine("GetPathForLoad:fileDialogInfo.CustomInitialDirectory"+fileDialogInfo.CustomInitialDirectory);
					// Console.WriteLine("GetPathForLoad:fileDialog.Directory"+fileDialog.Directory);

					foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Form, DialogResult>.FILTER_SEPARATOR))
					{
						string[] nameAndPattern = filter.Split(FileDialogInfo<Form, DialogResult>.FILTER_ITEM_SEPARATOR);
						openFileDialog.AddFilter(nameAndPattern[0], nameAndPattern[1]);
					}

					DialogResult response = await openFileDialog.ShowDialog(fileDialogInfo.Parent);

					if (response != DialogResult.None)
					{
						if (response == DialogResult.OK)
						{
							fileDialogInfo.Filename = openFileDialog.FileName;
							fileDialogInfo.Filenames = [openFileDialog.FileName];
						}
						fileDialogInfo.Response = response;
						returnValue = true;
					}
					else
					{
						//treat as Cancel
						fileDialogInfo.Response = DialogResult.Cancel;
						returnValue = true;
					}
				}
			}
			catch (Exception ex)
			{
				fileDialogInfo.ErrorMessage = ex.Message;
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

				throw;
			}

			fileDialogInfo.BoolResult = returnValue;
            return fileDialogInfo;
        }

        /// <summary>
        /// Select a folder path.
        /// Static. Calls Modern.Forms.FolderBrowserDialog.
        /// </summary>
        /// <param name="fileDialogInfo">returns path in Filename property if fileDialogInfo</param>
        /// <returns>fileDialogInfo</returns>
        public async static Task<FileDialogInfo<Form, DialogResult>> GetFolderPath
        (
            FileDialogInfo<Form, DialogResult> fileDialogInfo
        )
        {
            bool returnValue = default;
			try
			{
				FolderBrowserDialog folderDialog =
                    new()
                    {
                        Title = fileDialogInfo.Title,
                        InitialDirectory = string.IsNullOrEmpty(fileDialogInfo.Filename) ? Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator() : fileDialogInfo.Filename
                    };

				DialogResult response = await folderDialog.ShowDialog(fileDialogInfo.Parent);

				if (response != DialogResult.None)
                {
                    if (response == DialogResult.OK)
                    {
                        fileDialogInfo.Filename = folderDialog.SelectedPath;
                    }
                    fileDialogInfo.Response = response;
                    returnValue = true;
                }
                else
                {
                    //treat as Cancel
                    fileDialogInfo.Response = DialogResult.Cancel;
                    returnValue = true;
                }
			}
            catch (Exception ex)
            {
                fileDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return fileDialogInfo;
        }

        /// <summary>
        /// Shows an About dialog.
        /// non-Static. Calls MessageBox.Modern.Forms GetMessageBoxCustomWindow.
        /// </summary>
        /// <param name="aboutDialogInfo">AboutDialogInfo<Form, DialogResult, SKBitmap></param>
        /// <returns>AboutDialogInfo<Form, DialogResult, SKBitmap></returns>
        public async static Task<AboutDialogInfo<Form, DialogResult, SKBitmap>> ShowAboutDialog
        (
            AboutDialogInfo<Form, DialogResult, SKBitmap> aboutDialogInfo
        )
        {
            bool returnValue = default;
			try
			{
				string messageTemp =
		aboutDialogInfo.Title + "\n" +
		"ProgramName: " + aboutDialogInfo.ProgramName + "\n" +
		"Version: : " + aboutDialogInfo.Version + "\n" +
		"Copyright: " + aboutDialogInfo.Copyright + "\n" +
		"Comments: : " + aboutDialogInfo.Comments + "\n" +
		"Website: : " + aboutDialogInfo.Website;

				DialogBoxForm aboutDialog = new(DialogBoxForm.ButtonTypes.OK);
				aboutDialog.TitleBar.Text = "About...";
                aboutDialog.Message = messageTemp;

				DialogResult response = await aboutDialog.ShowDialog(aboutDialogInfo.Parent);

				if (response != DialogResult.None)
                {
                    aboutDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                aboutDialogInfo.ErrorMessage = ex.Message;
                //errorMessage = ex.Message; //error CS1988: Async methods cannot have ref, in or out parameters
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            aboutDialogInfo.BoolResult = returnValue;
            return aboutDialogInfo;
        }

        /// <summary>
        /// Get a printer.
        /// Static.
        /// </summary>
        /// <param name="printerDialogInfo">ref PrinterDialogInfo<Form, DialogResult, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<Form, DialogResult, string> printerDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            // ? printerDialog = null;
            // string response = null;

            try
            {
                // printerDialog = 
                //     new ?
                //     (
                //         printerDialogInfo.Title, 
                //         printerDialogInfo.Parent
                //     );
                // printerDialog.Modal = printerDialogInfo.Modal;

                // response = (ResponseType)printerDialog.Run();
                // if (response != ResponseType.None)
                // {
                //     if (response == ResponseType.Ok)
                //     {
                //         printerDialogInfo.Printer = printerDialog.SelectedPrinter;
                //         printerDialogInfo.Name = printerDialog.SelectedPrinter.Name;
                //     }

                //     printerDialogInfo.Response = response;
                    returnValue = true;
                // }
                throw new NotImplementedException("GetPrinter");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            printerDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Shows a message dialog.
        /// non-Static. Calls MessageBox.Modern.Forms GetMessageBoxStandardWindow.
        /// </summary>
        /// <param name="messageDialogInfo">MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes></param>
        /// <returns>Task<MessageDialogInfo<Form, DialogResult, object,  SKBitmap, DialogBoxForm.ButtonTypes>></returns>
        public async static Task<MessageDialogInfo<Form, DialogResult, object,  SKBitmap, DialogBoxForm.ButtonTypes>> ShowMessageDialog
        (
            MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> messageDialogInfo
        )
        {
            bool returnValue = default;
			try
			{
				//MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> returnValue = null;
				DialogBoxForm messageDialog = new(messageDialogInfo.ButtonsType, messageDialogInfo.MessageType);
				messageDialog.TitleBar.Text = messageDialogInfo.Title;
                messageDialog.Message = messageDialogInfo.Message;

				Console.WriteLine("ShowMessageDialog,new dlg,info set,parent=" + (messageDialogInfo.Parent==null));
				DialogResult response = await messageDialog.ShowDialog(messageDialogInfo.Parent);

				if (response != DialogResult.None)
                {
                    //return complex responses in dialoginfo
                    messageDialogInfo.Response = response;
                    returnValue = true;
                }
                messageDialogInfo.Response = response;
            }
            catch (Exception ex)
            {
                messageDialogInfo.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            messageDialogInfo.BoolResult = returnValue;
            return messageDialogInfo;
        }

        /// <summary>
        /// Get a color.
        /// Static. Calls Modern.Forms.ColorChooserDialog.
        /// </summary>
        /// <param name="colorDialogInfo">ref ColorDialogInfo<Form, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<Form, string, string> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            // Modern.Forms.ColorChooserDialog colorChooserDialog = null;
            // string response = null;
            // string localRGBA;

            try
            {
                // colorDialogInfo.Color = null;
                // colorChooserDialog = 
                //     new ColorChooserDialog
                //     (
                //         colorDialogInfo.Title, 
                //         colorDialogInfo.Parent 
                //     );
                // colorChooserDialog.Modal = colorDialogInfo.Modal;

                // response = (string)colorChooserDialog.Run();
                // if (!string.IsNullOrWhiteSpace(response))
                // {
                //     if (response == "ResponseType."Ok")
                //     {
                //         localRGBA = colorDialogInfo.Color;
                //         localRGBA.Red = colorChooserDialog.Rgba.Red;
                //         localRGBA.Green = colorChooserDialog.Rgba.Green;
                //         localRGBA.Blue = colorChooserDialog.Rgba.Blue;
                //         colorDialogInfo.Color = localRGBA;
                //         // Console.WriteLine
                //         // (
                //         //     string.Format
                //         //     (
                //         //         "Red:{0},Green:{1},Blue:{2}", 
                //         //         colorDialogInfo.Color.Red.ToString(),
                //         //         colorDialogInfo.Color.Green.ToString(),
                //         //         colorDialogInfo.Color.Blue.ToString()
                //         //     )
                //         // );
                //         // Console.WriteLine
                //         // (
                //         //     string.Format
                //         //     (
                //         //         "Red:{0},Green:{1},Blue:{2}", 
                //         //         colorChooserDialog.Rgba.Red.ToString(),
                //         //         colorChooserDialog.Rgba.Green.ToString(),
                //         //         colorChooserDialog.Rgba.Blue.ToString()
                //         //     )
                //         // );
                //     }
                //     colorDialogInfo.Response = response;
                    returnValue = true;
                //}
                throw new NotImplementedException("GetColor");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            colorDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static. Calls Modern.Forms.FontChooserDialog.
        /// </summary>
        /// <param name="fontDialogInfo">ref FontDialogInfo<Form, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFont
        (
            ref FontDialogInfo<Form, string, string> fontDialogInfo,//string fontDescription,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            // Modern.Forms.FontChooserDialog fontChooserDialog = null;
            // string response = null;

            try
            {
            //     // fontResponse = null;
            //     fontChooserDialog = 
            //         new FontChooserDialog
            //         (
            //             fontDialogInfo.Title, 
            //             fontDialogInfo.Parent
            //         );
            //     fontChooserDialog.Modal = fontDialogInfo.Modal;

            //     response = (string)fontChooserDialog.Run();
            //     if (!string.IsNullOrWhiteSpace(response))
            //     {
            //         if (response == "Ok")
            //         {
            //             fontDialogInfo.FontDescription = fontChooserDialog.FontDesc;
            //         }
            //         fontDialogInfo.Response = response;
                     returnValue = true;
            //     }
                throw new NotImplementedException("GetFont");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            fontDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Perform input of connection string and provider name.
        /// Uses MS Data Connections Dialog.
        /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        /// </summary>
        /// <param name="connectionString">ref string</param>
        /// <param name="providerName">ref string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetDataConnection
        (
            ref string connectionString,
            ref string providerName,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
            //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

            try
            {
                //dataConnectionDialog = new DataConnectionDialog();

                //DataSource.AddStandardDataSources(dataConnectionDialog);

                //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
                //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

                //dataConnectionConfiguration = new DataConnectionConfiguration(null);
                //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

                //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data CConnection Dialog.
                //dataConnectionDialog.ConnectionString = connectionString;

                if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
                {
                    ////extract connection string
                    //connectionString = dataConnectionDialog.ConnectionString;
                    //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

                    ////writes provider selection to xml file
                    //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

                    ////save these too
                    //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
                    //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
