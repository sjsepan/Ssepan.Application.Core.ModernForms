﻿using System.Collections.Generic;
using System.IO;
using SkiaSharp;

namespace Ssepan.Application.Core.ModernForms
{
	public static class ImageLoader
    {
        private static readonly Dictionary<string, SKBitmap> _cache = [];
        private const string _defaultLocation = "Resources";

        public static SKBitmap Get(string filename)
        {
            if (!_cache.ContainsKey(filename.ToLowerInvariant()))
            {
                _cache.Add(filename.ToLowerInvariant(), SKBitmap.Decode(Path.Combine(_defaultLocation, filename)));
            }

            return _cache[filename.ToLowerInvariant()];
        }
    }
}
