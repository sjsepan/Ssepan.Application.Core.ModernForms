# readme.md - README for Ssepan.Application.Core.ModernForms v5.2

## TODO

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.2:
~Initial release
~Cloned from Ssepan.Application.Core.* lib, numbered for parity

## Contact

Stephen J Sepan
<sjsepan@yahoo.com>
3/6/2024
