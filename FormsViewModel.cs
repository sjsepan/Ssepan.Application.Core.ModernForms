﻿#pragma warning disable IDE0034 //simplify new
#pragma warning disable IDE1005 //simplify delegate invocation
#pragma warning disable RCS1085 //

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Modern.Forms;
using SkiaSharp;

namespace Ssepan.Application.Core.ModernForms
{
	/// <summary>
	/// Note: this class can be subclassed without type parameters in the client.
	/// </summary>
	/// <typeparam name="TIcon">TIcon</typeparam>
	/// <typeparam name="TSettings">TSettings</typeparam>
	/// <typeparam name="TModel">TModel</typeparam>
	/// <typeparam name="TView">TView</typeparam>
	public class FormsViewModel
    <
        TIcon,
        TSettings,
        TModel,
        TView
    > :
        ViewModelBase<TIcon>
        where TIcon : class
        where TSettings : class, ISettings, new()
        where TModel : class, IModel, new()
        where TView : Form
    {
        #region Declarations
        //public delegate bool DoWork_WorkDelegate(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);
        public delegate TReturn DoWork_WorkDelegate<TReturn>(BackgroundWorker worker, DoWorkEventArgs e, ref string errorMessage);

        protected static FileDialogInfo<Form, DialogResult> _settingsFileDialogInfo = default(FileDialogInfo<Form, DialogResult>);
        protected Dictionary<string, TIcon> _actionIconImages;

        #endregion Declarations

        #region Constructors
        public FormsViewModel()
        {
            if (SettingsController<TSettings>.Settings == null)
            {
                SettingsController<TSettings>.New();
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo
        ) :
            this()
        {
            try
            {
                //(and the delegate it contains
                if (propertyChangedEventHandlerDelegate != default)
                {
                    PropertyChanged += new PropertyChangedEventHandler(propertyChangedEventHandlerDelegate);
                }

                _actionIconImages = actionIconImages;

                _settingsFileDialogInfo = settingsFileDialogInfo;

                ActionIconImage = _actionIconImages["Save"];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public FormsViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, TIcon> actionIconImages,
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo,
            TView view //= default(TView) //(In VB 2010, )VB caller cannot differentiate between members which differ only by an optional param--SJS
        ) :
            this(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
                View = view;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
		#endregion Constructors

		#region Properties
		public TView View { get; set; }
		#endregion Properties

		#region Methods
		#region Menus
		#region menu feature boilerplate
		//may be used as-is, or overridden to handle differently

		public async virtual Task FileNew()
        {
            MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "New" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["New"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    questionMessageDialogInfo =
                        new MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes>
                        (
                            View,//saved Window reference
                            true,
                            "New",
                            null,
                            ImageLoader.Get("Info.png"),//Question is not available
                            DialogBoxForm.ButtonTypes.YesNo,
                            "Save changes?",
                            DialogResult.None
                        );

                    questionMessageDialogInfo = await Dialogs.ShowMessageDialog(questionMessageDialogInfo);
                    if (!questionMessageDialogInfo.BoolResult)
                    {
                        errorMessage = questionMessageDialogInfo.ErrorMessage;
                        throw new ApplicationException(errorMessage);
                    }

                    switch (questionMessageDialogInfo.Response)
                    {
						case DialogResult.Yes:
							//SAVE
							await FileSaveAs();

							break;

						case DialogResult.No:
							break;

						default:
							throw new InvalidEnumArgumentException();
					}
                }

                //NEW
                if (!SettingsController<TSettings>.New())
                {
                    throw new ApplicationException(string.Format("Unable to get New settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar("", string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// Open object at SettingsController<TSettings>.FilePath.
        /// </summary>
        /// <param name="forceDialog">If false, just use SettingsController<TSettings>.FilePath</param>
        public async virtual Task FileOpen(bool forceDialog = true)
        {
            MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Opening" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Open"],
                    true,
                    33
                );

                if (SettingsController<TSettings>.Settings.Dirty)
                {
                    //prompt before saving
                    questionMessageDialogInfo =
                        new MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes>
                        (
                            View,//saved Window reference
                            true,
                            "Open",
                            null,
                            ImageLoader.Get("Info.png"),//Question is not available
                            DialogBoxForm.ButtonTypes.YesNo,
                            "Save changes?",
                            DialogResult.None
                        );

                    questionMessageDialogInfo = await Dialogs.ShowMessageDialog(questionMessageDialogInfo);
                    if (!questionMessageDialogInfo.BoolResult)
                    {
                        errorMessage = questionMessageDialogInfo.ErrorMessage;
                        throw new ApplicationException(errorMessage);
                    }

                    if (questionMessageDialogInfo.Response != DialogResult.None)
                    {
                        if (questionMessageDialogInfo.Response == DialogResult.Yes)
                        {
                            //SAVE
                            await FileSave();
                        }
                        else
                        {
                            //treat as cancel
                        }
                    }
                    else
                    {
                        //treat as cancel
                    }
                }

                if (forceDialog)
                {
                    _settingsFileDialogInfo.Title = "Open";
                    _settingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;

                    _settingsFileDialogInfo = await Dialogs.GetPathForLoad(_settingsFileDialogInfo);

                    if (!_settingsFileDialogInfo.BoolResult)
                    {
                        errorMessage = _settingsFileDialogInfo.ErrorMessage;
                        throw new ApplicationException(string.Format("GetPathForLoad: {0}", errorMessage));
                    }

                    if (_settingsFileDialogInfo.Response != DialogResult.None)
                    {
                        if (_settingsFileDialogInfo.Response == DialogResult.OK)
                        {
                            SettingsController<TSettings>.FilePath = _settingsFileDialogInfo.Filename;
                            UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                        }
                        else
                        {
                            //treat as cancel
                            StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                            return; //if open was cancelled
                        }
                    }
                    else
                    {
                        //treat as cancel
                            StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                            return; //if open was cancelled
                    }
                }

                //OPEN
                if (!SettingsController<TSettings>.Open())
                {
                    throw new ApplicationException(string.Format("Unable to Open settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual async Task FileSave(bool isSaveAs=false)
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "Saving" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Save"],
                    true,
                    33
                );

                _settingsFileDialogInfo.Title = isSaveAs ? "Save As..." : "Save...";
                _settingsFileDialogInfo.ForceDialog = isSaveAs;
                _settingsFileDialogInfo.Modal = true;
                _settingsFileDialogInfo.Filename = SettingsController<TSettings>.FilePath;
                _settingsFileDialogInfo = await Dialogs.GetPathForSave(_settingsFileDialogInfo);

                if (!_settingsFileDialogInfo.BoolResult)
                {
                    errorMessage = _settingsFileDialogInfo.ErrorMessage;
                    throw new ApplicationException(string.Format("GetPathForSave: {0}", errorMessage));
                }

                if (_settingsFileDialogInfo.Response != DialogResult.None)
                {
                    if (_settingsFileDialogInfo.Response == DialogResult.OK)
                    {
                        SettingsController<TSettings>.FilePath = _settingsFileDialogInfo.Filename;
                        UpdateStatusBarMessages(StatusMessage + _settingsFileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if save was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if save was cancelled
                }

                //SAVE
                if (!SettingsController<TSettings>.Save())
                {
                    throw new ApplicationException(string.Format("Unable to Save settings.\r\nPath: {0}", SettingsController<TSettings>.FilePath));
                }

                ModelController<TModel>.Model.Refresh();

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        public virtual async Task FileSaveAs()
        {
            try
            {
                await FileSave(true);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// Not implemented, use FileExitAsync instead
        /// </summary>
        public async virtual Task<bool> FileExit()
        {
            bool resultCancelQuitting = false;//default to false, which means do not Cancel (i.e. - do Close)
            MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes> questionMessageDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Quit" + ACTION_IN_PROGRESS,
                    null,
                    null,
                    true,
                    33
                );

                // prompt before quitting
                questionMessageDialogInfo =
                    new MessageDialogInfo<Form, DialogResult, object, SKBitmap, DialogBoxForm.ButtonTypes>
                    (
                        parent: View,//saved Window reference
                        modal: true,
                        title: "Quit?",
                        dialogFlags: null,
                        messageType: ImageLoader.Get("Question.png"),
                        buttonsType: DialogBoxForm.ButtonTypes.YesNo,
                        message: "Are you sure you want to quit?",
                        response: DialogResult.None
                    );

                questionMessageDialogInfo = await Dialogs.ShowMessageDialog(questionMessageDialogInfo);
                if (!questionMessageDialogInfo.BoolResult)
                {
					throw new ApplicationException(questionMessageDialogInfo.ErrorMessage);
                }

                if (questionMessageDialogInfo.Response == DialogResult.No)
                {
                    resultCancelQuitting = true;
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("{0}", ex.Message);

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
             }
            return resultCancelQuitting;
        }

        public async virtual void HelpAbout<TAssemblyInfo>()
            where TAssemblyInfo :
            //class,
            AssemblyInfoBase<Form>,
            new()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            TAssemblyInfo assemblyInfo = null;
            AboutDialogInfo<Form, DialogResult, SKBitmap> aboutDialogInfo = null;//TResponseEnum has to be string, because About dialog uses custom dialog
            string errorMessage = null;

            try
            {
                StartProgressBar
                (
                    "About" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["About"],
                    true,
                    33
                );

                assemblyInfo = new TAssemblyInfo();
                // use GUI mode About feature
                aboutDialogInfo = new AboutDialogInfo<Form, DialogResult, SKBitmap>()
                {
                    Parent = View,
                    Response = DialogResult.None,
                    Modal = true,
                    Title = "About...",
                    ProgramName = assemblyInfo.Title,//"MvcForms.Core.Modern.Forms",
                    Version = assemblyInfo.Version,//"v0.7",
                    Copyright = assemblyInfo.Copyright,//"Copyright (C) 1989, 1991 Free Software Foundation, Inc.  \n59 Temple Place - Suite 330, Boston, MA  02111-1307, USA",
                    Comments = assemblyInfo.Description,//"Desktop GUI app prototype, on Linux, in C# / DotNet[5|6] / Modern.Forms, using VSCode / Glade.",
                    Website = assemblyInfo.Website,//"https://gitlab.com/sjsepan/MvcForms.Core.Modern.Forms",
                    Logo = ImageLoader.Get("App.png")
                };

                //call async instance version
                aboutDialogInfo = await Dialogs.ShowAboutDialog(aboutDialogInfo);
                if (aboutDialogInfo.ErrorMessage != null)//put an errorMessage field in dialogInfo base for passing back
                {
                    errorMessage = aboutDialogInfo.ErrorMessage;
                    throw new ApplicationException(errorMessage);
                }

                //no need to check response=OK, etc

                StopProgressBar("About completed.");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        #endregion menu feature boilerplate

        #region menu feature implementation
        //likely to need overridden to handle feature specifics

        #endregion menu feature implementation

        #endregion Menus

        #region Controls

        /// <summary>
        /// Handle DoWork event.
        /// </summary>
        /// <typeparam name="TReturn">TReturn</typeparam>
        /// <param name="worker">BackgroundWorker</param>
        /// <param name="e">DoWorkEventArgs</param>
        /// <param name="workDelegate">DoWork_WorkDelegate<TReturn></param>
        /// <param name="resultNullDescription">string</param>
        public virtual void BackgroundWorker_DoWork<TReturn>
        (
            BackgroundWorker worker,
            DoWorkEventArgs e,
            DoWork_WorkDelegate<TReturn> workDelegate,
            string resultNullDescription = "No result was returned."
        )
        {
            string errorMessage = default(string);

            try
            {
                //run process
                if (workDelegate != null)
                {
                    e.Result =
                        workDelegate
                        (
                            worker,
                            e,
                            ref errorMessage
                        );
                }

                //look for specific problem
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    throw new Exception(errorMessage);
                }

                //warn about unexpected result
                if (e.Result == null)
                {
                    throw new Exception(resultNullDescription);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //re-throw and let RunWorkerCompleted event handle and report error.
                throw;
            }
        }

        /// <summary>
        /// Handle ProgressChanged event.
        /// </summary>
        /// <param name="description">string</param>
        /// <param name="userState">object, specifically a string.</param>
        /// <param name="progressPercentage">int</param>
        public virtual void BackgroundWorker_ProgressChanged
        (
            string description,
            object userState,
            int progressPercentage
        )
        {
            string message = string.Empty;

            try
            {
                if (userState != null)
                {
                    message = userState.ToString();
                }
                UpdateProgressBar(string.Format("{0} ({1})...{2}%", description, message, progressPercentage.ToString()), progressPercentage);
                //System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Handle RunWorkerCompleted event.
        /// </summary>
        /// <param name="description">string</param>
        /// <param name="worker">BackgroundWorker</param>
        /// <param name="e">RunWorkerCompletedEventArgs</param>
        /// <param name="errorDelegate">Action<Exception>. Replaces default behavior of displaying the exception message.</param>
        /// <param name="cancelledDelegate">Action. Replaces default behavior of displaying a cancellation message. Handles the display message only; differs from cancelDelegate, which handles view-level behavior not specific to this worker.</param>
        /// <param name="completedDelegate">Action. Extends default behavior of refreshing the display; execute prior to Refresh().</param>
        public virtual void BackgroundWorker_RunWorkerCompleted
        (
            string description,
            BackgroundWorker worker,
            RunWorkerCompletedEventArgs e,
            Action<Exception> errorDelegate = null,
			Action cancelledDelegate = null,
			Action completedDelegate = null
        )
        {
            try
            {
                Exception error = e.Error;
                bool isCancelled = e.Cancelled;
                object result = e.Result;

                // First, handle the case where an exception was thrown.
                if (error != null)
                {
                    if (errorDelegate != null)
                    {
                        errorDelegate(error);
                    }
                    else
                    {
                        // Show the error message
                        StopProgressBar(null, error.Message);
                    }
                }
                else if (isCancelled)
                {
                    if (cancelledDelegate != null)
                    {
                        cancelledDelegate();
                    }
                    else
                    {
                        // Handle the case where the user cancelled the operation.
                        StopProgressBar(null, "Cancelled.");

                        //if (View.cancelDelegate != null)
                        //{
                        //    View.cancelDelegate();
                        //}
                    }
                }
                else
                {
                    // Operation completed successfully, so display the result.
                    if (completedDelegate != null)
                    {
                        completedDelegate();
                    }

                    //backgroundworker calls New/Save without UI refresh; refresh UI explicitly here.
                    ModelController<TModel>.Model.Refresh();
                }

                // Do post completion operations, like enabling the controls etc.
                View.Show();

                // Inform the user we're done
                StopProgressBar(description, null);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                ////clear cancellation hook
                //View.cancelDelegate = null;
            }
        }

        // /// <summary>
        // ///
        // /// </summary>
        // public void BindingSource_PositionChanged<TItem>(BindingSource bindingSource, EventArgs e, Action<TItem> itemDelegate)
        // {
        //     try
        //     {
        //         TItem current = (TItem)bindingSource.Current;

        //         if (current != null)
        //         {
        //             if (itemDelegate != null)
        //             {
        //                 itemDelegate(current);
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        // /// <summary>
        // /// Handle DataError event; specifically, catch combobox cell data errors.
        // /// This allows validation message to be displayed
        // ///  and still allows the user to fix and save the settings.
        // /// </summary>
        // /// <param name="grid"></param>
        // /// <param name="e"></param>
        // /// <param name="cancel"></param>
        // /// <param name="throwException"></param>
        // /// <param name="logException"></param>
        // public void Grid_DataError
        // (
        //     DataGridView grid,
        //     DataGridViewDataErrorEventArgs e,
        //     bool? cancel,
        //     bool? throwException,
        //     bool logException
        // )
        // {
        //     try
        //     {
        //         if (cancel.HasValue)
        //         {
        //             //cancel to prevent default DataError dialogs
        //             e.Cancel = true;
        //         }

        //         if (throwException.HasValue)
        //         {
        //             //there are cases where you do not want to throw exception; because it will drop application immediately with only a log
        //             e.ThrowException = throwException.Value;
        //         }

        //         if (logException)
        //         {
        //             Log.Write(e.Exception, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }
        #endregion Controls

        #region Utility

        /// <summary>
        /// Manage buttons' state while processes are running.
        /// Usage:
        ///     View.permitEnabledStateXxx = ButtonEnabled(enabledFlag, View.permitEnabledStateXxx, View.cmdXxx, View.menuFileXxx, View.buttonFileXxx);
        /// </summary>
        /// <param name="enabledFlag">bool</param>
        /// <param name="permitEnabledState">bool</param>
        /// <param name="button">Button</param>
        /// <param name="menuItem">MenuItem</param>
        /// <param name="buttonItem">Button</param>
        /// <returns>bool. updated remembered state</returns>
        public bool ButtonEnabled
        (
            bool enabledFlag,
            bool permitEnabledState,
            Button button,
            MenuItem menuItem = null,
            Button buttonItem = null
        )
        {
            bool returnValue = permitEnabledState; // default(bool);
            try
            {
                if (enabledFlag)
                {
                    //ENABLING
                    //recall state
                    if (button != null)
                    {
                        button.Enabled = permitEnabledState;
                    }
                    if (menuItem != null)
                    {
                        menuItem.Enabled = permitEnabledState;
                    }
                    if (buttonItem != null)
                    {
                        buttonItem.Enabled = permitEnabledState;
                    }
                }
                else
                {
                    //DISABLING
                    //remember state
                    if (button != null)
                    {
                        returnValue = button.Enabled;
                    }
                    else if (menuItem != null)
                    {
                        returnValue = menuItem.Enabled;
                    }
                    else if (buttonItem != null)
                    {
                        returnValue = buttonItem.Enabled;
                    }

                    //disable
                    if (button != null)
                    {
                        button.Enabled = enabledFlag;
                    }
                    if (menuItem != null)
                    {
                        menuItem.Enabled = enabledFlag;
                    }
                    if (buttonItem != null)
                    {
                        buttonItem.Enabled = enabledFlag;
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Utility
        #endregion Methods

    }
}
