﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core.ModernForms
{
    public static class PlugIn
    {
        public const string PLUGIN_FILE_MASK = "*.dll";

        public static List<TInterface> GetInterfacesImplementedAtPath<TInterface>
        (
            string plugInPath,
            string plugInFilter,
            string[] ignoreCommonLibraries
        )
            where TInterface : class
        {
            List<TInterface> returnValue = default;
			try
			{
                returnValue = [];

				foreach (string filepath in Directory.GetFiles(plugInPath, plugInFilter))
                {
                    //try-catch individual attempts, so one bad plugin does not prevent loading of good plugins
                    try
                    {
                        //Note: Trap references to common assembly before calling this method. 
                        // Calls to IsAssignableFrom will fail for valid implementation classes if previously called 
                        // with class containing interface *definition* and *base class*. (Or maybe its because its a 
                        // referenced class already loaded by host.)
                        if (!ignoreCommonLibraries.Contains(Path.GetFileNameWithoutExtension(filepath)))
                        {
                            Assembly assembly = GetAssembly(filepath);
							IEnumerable<TInterface> matchingClasses = InstancesOf<TInterface>(assembly);
							returnValue.AddRange(matchingClasses);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        /// <summary>
        ///
        /// based on code by 'wrack' on http://www.neowin.net/forum/topic/1110005-c-better-more-efficient-way-to-load-dll-dynamically-get-interface-instance/page__pid__595224399#entry595224399
        /// </summary>
        /// <param name="filepath">string</param>
        /// <returns>Assembly</returns>
        public static Assembly GetAssembly(string filepath)
        {
            Assembly returnValue = default;

            try
            {
                if (Path.IsPathRooted(filepath)) //suggested by Aethec on http://www.neowin.net/forum/topic/1110005-c-better-more-efficient-way-to-load-dll-dynamically-get-interface-instance/page__pid__595224399#entry595224399
                {
                    returnValue = Assembly.LoadFrom(filepath);
                }
                else
                {
                    returnValue = Assembly.LoadFrom(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), filepath));
                }

                if (returnValue == null)
                {
                    throw new ApplicationException(string.Format("Unable to load assembly: {0}", filepath));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Trap references to common assembly before calling this method.
        /// *based on* code by Matt Hamilton on http://madprops.org/about-matt-hamilton/
        /// </summary>
        /// <typeparam name="TInterface">TInterface</typeparam>
        /// <returns>IEnumerable<TInterface></returns>
        public static IEnumerable<TInterface> InstancesOf<TInterface>(Assembly assembly)
            where TInterface : class
        {
            IEnumerable<TInterface> returnValue = default;
            try
            {
                Type interfaceType = typeof(TInterface);
                returnValue =
                    from exportedType in assembly.GetExportedTypes()
                    where
                        exportedType.IsClass
                        &&
                        !exportedType.IsInterface
                        &&
                        !exportedType.IsAbstract
                        &&
                        interfaceType.IsAssignableFrom(exportedType) //Note:will fail for valid implementation classes if previously called with class containing interface *definition* and *base class*. (Or maybe its because its a referenced class already loaded by host.) Trap references to common assembly before calling this method.
                        &&
                        exportedType.GetConstructor(Type.EmptyTypes) != null
                    select Activator.CreateInstance(exportedType) as TInterface;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
    }
}
