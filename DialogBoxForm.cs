using System;
using System.Drawing;
using System.Linq;
using Modern.Forms;

namespace Ssepan.Application.Core.ModernForms
{
	/// <summary>
	/// Represents a popup dialog used to inform the user of a message and solicit a decision.
	/// </summary>
	public class DialogBoxForm : Form//can't inherit from MessageBoxForm, because members are private instead of protected
    {
		private readonly PictureBox icon;
		private readonly TextBox label;
        private readonly Button ok_button;
        private readonly Button cancel_button;
        private readonly Button yes_button;
        private readonly Button no_button;
        private readonly Button abort_button;
        private readonly Button retry_button;
        private readonly Button ignore_button;

        public enum ButtonTypes
        {
            OK,
            OKCancel,
            YesNo,
            YesNoCancel,
            AbortRetryIgnore
        }

        /// <summary>
        /// Initializes a new instance of the DialogBoxForm class.
        /// </summary>
        public DialogBoxForm (ButtonTypes buttonType = ButtonTypes.OK, SkiaSharp.SKBitmap messageIcon = null)
        {
            StartPosition = FormStartPosition.CenterParent;
            AllowMinimize = false;
            AllowMaximize = false;

			ButtonType = buttonType;

			label = Controls.Add (new TextBox {
                Dock = DockStyle.Fill,
                Height = 105,
                MultiLine = true,
                ReadOnly = true,
                Padding = new Padding (10)
            });

			//Left must be after Fill
			icon = Controls.Add(new PictureBox {
				Dock = DockStyle.Left,
				Height = 32,
				Width = 32,
				Padding = new Padding(5),
				Image = messageIcon
			}
			);

            var label_panel = Controls.Add (new Panel {
                Dock = DockStyle.Bottom,
                Height = 45
            });

            label.Style.BackgroundColor = Theme.FormBackgroundColor;
            label.Style.Border.Width = 0;

			if (ButtonType == ButtonTypes.YesNo || ButtonType == ButtonTypes.YesNoCancel)
			{
				yes_button = label_panel.Controls.Add
				(
				    new Button
				    {
				        Text = "Yes",
				        Dock = DockStyle.Right
				    }
				);
				yes_button.Click +=
				    (o, e) =>
				    {
				        DialogResult = DialogResult.Yes;
				        Close();
				    };

				no_button = label_panel.Controls.Add
				(
				    new Button
				    {
				        Text = "No",
				        Dock = DockStyle.Right
				    }
				);
				no_button.Click +=
				    (o, e) =>
				    {
				        DialogResult = DialogResult.No;
				        Close();
				    };
			}

			if (ButtonType==ButtonTypes.OK || ButtonType==ButtonTypes.OKCancel)
			{
				ok_button = label_panel.Controls.Add
				(
					new Button
					{
						Text = "OK",
						Dock = DockStyle.Right
					}
				);
				ok_button.Click +=
					(o, e) =>
					{
						DialogResult = DialogResult.OK;
						Close();
					};
			}

			if (ButtonType == ButtonTypes.OKCancel || ButtonType == ButtonTypes.YesNoCancel)
			{
				cancel_button = label_panel.Controls.Add
				(
					new Button
					{
						Text = "Cancel",
						Dock = DockStyle.Right
					}
				);
				cancel_button.Click +=
					(o, e) =>
					{
						DialogResult = DialogResult.Cancel;
						Close();
					};
			}

			if (ButtonType == ButtonTypes.AbortRetryIgnore)
			{
				abort_button = label_panel.Controls.Add
				(
				    new Button
				    {
				        Text = "Abort",
				        Dock = DockStyle.Right
				    }
				);
				abort_button.Click +=
				    (o, e) =>
				    {
				        DialogResult = DialogResult.Abort;
				        Close();
				    };

				retry_button = label_panel.Controls.Add
				(
				    new Button
				    {
				        Text = "Retry",
				        Dock = DockStyle.Right
				    }
				);
				retry_button.Click +=
				    (o, e) =>
				    {
				        DialogResult = DialogResult.Retry;
				        Close();
				    };

				ignore_button = label_panel.Controls.Add
				(
				    new Button
				    {
				        Text = "Ignore",
				        Dock = DockStyle.Right
				    }
				);
				ignore_button.Click +=
				    (o, e) =>
				    {
				        DialogResult = DialogResult.Ignore;
				        Close();
				    };
			}

			// ok_button.Click += (o, e) => Close ();
		}

        /// <summary>
        /// Initializes a new instance of the DialogBoxForm class with the specified title and message.
        /// </summary>
        public DialogBoxForm (string title, string message) : this ()
        {
            Text = title;
            label.Text = message;

            CalculateDialogSize ();
        }

        /// <inheritdoc/>
        protected override Size DefaultSize => new(400, 200);

        private void CalculateDialogSize ()
        {
            var num_lines = label?.Text?.Count (c => c == '\n') ?? 0;

            if (num_lines > 10)
                Size = new Size (800, 400);
            else if (num_lines > 4)
                Size = new Size (600, 300);
            else
                Size = new Size (400, 200);

            //ok_button.Left = (int)((Size.Width - ok_button.Width) / 2);
        }

        /// <summary>
        /// Gets or sets the message of the dialog.
        /// </summary>
        public string Message {
            get => label.Text;
            set {
                if (label.Text != value) {
                    label.Text = value;
                    CalculateDialogSize ();
                }
            }
        }
		public ButtonTypes ButtonType { get; set; } = ButtonTypes.OK;
	}
}
